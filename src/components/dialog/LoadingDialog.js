import React from 'react'
import BaseViewComponent from "../BaseViewComponent";
import DialogBase from "./DialogBase"

export default class LoadingDialog extends BaseViewComponent{

    constructor(props){
        super(props);
        this.state = {

        }
    }

    static show(options){
        DialogBase.show(<LoadingDialog {...options}/>)
    }

    static hidden(){
        DialogBase.hidden()
    }

    static defaultProps = {
        hitBackgroundHidden:false,
        loadingImage:'',
    }

    componentDidMount(){
        console.log('LoadingDialog-----------componentDidMount')
    }
    componentWillUnmount(){
        console.log('LoadingDialog-----------componentWillUnmount')
    }

    render (){
        const { hitBackgroundHidden ,loadingTitle,loadingImage} = this.props
        return (
            <div
                style={loadingDialogCss.background}
                onClick={()=>{
                    hitBackgroundHidden && LoadingDialog.hidden();
                }}
            >
                <div style={loadingDialogCss.center}>
                    <div style={{
                        width:400,height:400,flexDirection:'column',display:'flex',justifyContent:'center',
                        alignItems:'center',color:'white'
                    }}>
                        {loadingImage?<img alt={''} id="img1" style={{width:50,height:50}} src={loadingImage}></img>:null}
                        <div style={{height:15}}/>
                        {loadingTitle?<span>{loadingTitle}</span>:null}
                    </div>
                </div>
            </div>
        )
    }
}

const loadingDialogCss = {
    background:{
        position: "fixed",
        display: "flex",
        justifyContent:'center',
        alignItems:'center',
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        backgroundColor:"#00000050"
    },
    center:{
        position: "relative",
        margin:"auto",
        justifyContent:'center',
        alignItems:'center',
    }
}
