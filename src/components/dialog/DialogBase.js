import React from 'react'
import ReactDOM from 'react-dom';

export default class DialogBase{

    static Dialog_Key = 'loadingDialog-doc';
    static rootDiv = document.getElementById('root');

    static show(childrenElement){
        DialogBase.hidden();
        DialogBase.appendLoadingDialogModel(childrenElement);
    }

    static hidden(){
        if(document.getElementById(DialogBase.Dialog_Key)){
            // 移除ChildrenElement
            ReactDOM.render(<div />, document.getElementById('loadingDialog'));
            // 移除 dialog
            DialogBase.rootDiv.removeChild(document.getElementById(DialogBase.Dialog_Key));
        }
    }

    static appendLoadingDialogModel(ChildrenElement){
        if(!ChildrenElement) return;
        const newNode = document.createElement(DialogBase.Dialog_Key);
        newNode.id = DialogBase.Dialog_Key;
        newNode.innerHTML = '<div id="loadingDialog"></div>';
        DialogBase.rootDiv.appendChild(newNode)
        ReactDOM.render(ChildrenElement, document.getElementById('loadingDialog'));
    }

}


