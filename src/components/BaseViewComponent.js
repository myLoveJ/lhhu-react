import React, {
    PureComponent
} from 'react'
import AZTool from '../utility/AZTool'
import AZCatch from '../utility/AZCatch'
/**
 * BaseViewComponent    所有自定义控件的基类
 */
export default class BaseViewComponent extends PureComponent {

    /**
     * 可携带的自定义扩展数据
     */
    _tagObj = null; // 类似ios、android给视图打tag
    _moduleId = null; // 获取随机id，标记当前的Component,可视为为唯一

    // 构造
    constructor(props) {
        super(props);
        let { tag } = this.props;
        this._tagObj= tag;
        this._moduleId = AZTool.randomString(8);
    }

    componentDidCatch(error, info) {
        AZCatch.errorCatch && AZCatch.errorCatch(error,info);
    }

    /**
     * 设置tagObj
     */
    setTag(tagObj){
        this._tagObj = tagObj;
    }

    /**
     * 获取tag
     */
    getTag(){
        return this._tagObj;
    }

    getModuleId(){
        return this._moduleId
    }

}
