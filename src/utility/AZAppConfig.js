
function getDeviceType() {

    const ua = navigator.userAgent.toLocaleLowerCase()
    const agent = ["WeChat","Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
    let _device = 'Windows'
    for(let i=0; i< agent.length ; i++){
        const keyStr = agent[i]
        if(ua.indexOf(keyStr.toLocaleLowerCase())>0){
            _device = keyStr
            break;
        }
    }
    return _device
}

/**
 * 数组中是否包含item
 * @param argArray
 * @returns {boolean}
 */
Array.prototype.contains = function (item) {
    for (let index = 0; index < this.length; index++) {
        if (this[index] === item) {
            return true;
        }
    }
    return false;
};


window.__Device__  = getDeviceType()
window.__IOS__     = window.__Device__ === 'iPhone';
window.__ANDROID__ = window.__Device__ === 'Android';
window.__WINDOWS__ = window.__Device__ === 'Windows';

window.IS_IPHONE_X = window.__Device__ === "iPhone" &&  (window.screen.height === 812 || window.screen.height === 896)





export {

}
