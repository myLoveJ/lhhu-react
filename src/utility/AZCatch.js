/**
 * @author lhhu
 * @class AZCatch 全局错误捕捉
 * @description react 全局捕捉错误日志封装，持续更新中。。。
 * @description 1、window.onerror ，2、try cathc， 3、componentDidCatch（react 16版本以上）
 */
class AZCatch {
    static globalTryCatch = function (asyncFunc,catchFunc) {
        try {
            return asyncFunc()
        } catch (err) {
            AZCatch.errorCatch && AZCatch.errorCatch(err);
            catchFunc && catchFunc(err)
        }
    }

    static globalPromis = function (promiseFunc) {
        return new Promise((resolve,reject)=>{
            return promiseFunc().then(res=>{
                resolve(res);
            })
            .catch(err=>{
                AZCatch.errorCatch && AZCatch.errorCatch(err);
                reject(err);
            })
        })
    };
}

if(window){
    window.onerror = function (msg, url, line,colno,err) {
        AZCatch.errorCatch && AZCatch.errorCatch(err);
      }
}

function errorCatch(err,info) {
    
}
/**
 * @method 启用错误日志配置方法
 * @param {捕捉错误日志的方法} catchFunc ，替换原有的errorCatch方法
 */
function useCatch(catchFunc){
    Object.assign(AZCatch,{errorCatch:catchFunc});
}

AZCatch.errorCatch     = errorCatch
AZCatch.useCatch       = useCatch

export default AZCatch


