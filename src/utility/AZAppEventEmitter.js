

const getEmitterMap = function () {
    const _instance = Instance() || {}
    console.assert(_instance, "AppEventEmitter 未初始化")
    return _instance._eventEmitterMap
}

class AppEventEmitter {
    static emit = function (key,opt) {
        const _emitMap = getEmitterMap()
        const _callbackArr = _emitMap.get(key) || []
        _callbackArr.map(callback => {
            callback(opt)
        })
    }

    static addListener = function (key,callback) {
        const _emitMap = getEmitterMap()
        const _callbackArr = _emitMap.get(key) || []
        if (!_callbackArr.contains(callback)) {
            _callbackArr.push(callback)
            _emitMap.set(key,_callbackArr)
        }
    }

    static removeListener = function (key,callback) {
        const _emitMap = getEmitterMap()
        const _callbackArr = _emitMap.get(key) || []
        if (_callbackArr.contains(callback)) {
            const _newArr = _callbackArr.filter(o => o != callback) || []
            _emitMap.set(key,_newArr)
        }
    }

    constructor() {
        this._eventEmitterMap = new Map();
    }

}
const Instance = (function () {
    let serverTimeInstance = null;
    return function () {
        if (!serverTimeInstance) {
            serverTimeInstance = new AppEventEmitter()
        }
        return serverTimeInstance;
    }

})()


export default  AppEventEmitter;
