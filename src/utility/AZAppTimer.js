

const getEmitterMap = function () {
    const _instance = Instance() || {}
    console.assert(_instance, "AZAppTimer 未初始化")
    return _instance
}

class AZAppTimer {

    static addTimerListener = function (callback) {
        const _instance = getEmitterMap()
        _instance.addTimerListener(callback)

    }

    static removeTimerListener = function (callback) {
        const _instance = getEmitterMap()
        _instance.removeTimerListener(callback)
    }

    constructor() {
        this._callbacks = new Array();
        this._interval = null
    }

    addTimer = ()=>{
        if (this._callbacks.length > 0 && this._interval == null){
            // 没有定时器，且需要定时器的时候才创建定时器
            this._interval = setInterval(this._callBack,1000);
            console.log('AZAppTimer----开启定时器');
        }
    }

    cleanTimer = ()=>{
        if(this._interval && this._callbacks.length == 0){
            // 有定时器并且需要停止定时器
            clearInterval(this._interval)
            this._interval = null;
            console.log('AZAppTimer----关闭定时器');
        }
    }

    _callBack = ()=>{
        this._callbacks.map(callback => {
            callback()
        })
    }

    addTimerListener=(callback)=>{
        if (!this._callbacks.contains(callback)){
            this._callbacks.push(callback)
        }
        this.addTimer();
    }

    removeTimerListener=(callback)=>{
        if (this._callbacks.contains(callback)) {
            this._callbacks = this._callbacks.filter(o => o != callback)
        }
        this.cleanTimer()
    }
}
const Instance = (function () {
    let appTimer = null;
    return function () {
        if (!appTimer) {
            appTimer = new AZAppTimer()
        }
        return appTimer;
    }
})()
export default  AZAppTimer;
