import AZTool from './AZTool'
function isSupportedStorage (isLocal){
    const storage = isLocal?localStorage:sessionStorage;
    if (!storage) return false
    const testKey = 'test';
    try {
        storage.setItem(testKey, 'testValue');
        storage.removeItem(testKey);
        return true;
    } catch (error) {
        return false;
    }
}
function setItem(key,value,isLocal){
    if(!isSupportedStorage(isLocal)) return null
    const storage = isLocal?localStorage:sessionStorage;
    storage.setItem(key,JSON.stringify(value))
}
function clear(isLocal){
    if(!isSupportedStorage(isLocal)) return null
    const storage = isLocal?localStorage:sessionStorage;
    storage.clear();
}
function getItem(key,isLocal) {
    if(!isSupportedStorage(isLocal)) return null
    const storage = isLocal?localStorage:sessionStorage;
    const data = storage.getItem(key);
    if (AZTool.isNull_Undefined(data)){
        return null;
    }else {
        return JSON.parse(data);
    }
}
function removeItem(key,isLocal) {
    if(!isSupportedStorage(isLocal)) return null
    const storage = isLocal?localStorage:sessionStorage;
    storage.removeItem(key)
}

class AZStorage {}
AZStorage.setItem              = setItem
AZStorage.getItem              = getItem
AZStorage.clear                = clear
AZStorage.removeItem           = removeItem
AZStorage.isSupportedStorage   = isSupportedStorage
export default AZStorage