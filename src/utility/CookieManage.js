import  AZTool from './AZTool'

export default {
    // 设置cookie,默认是30天
    setCookie(key, value ,exTimes) {
        // key 一定要有
        if (!AZTool.isNotEmptyString(key)) return 
        if (!AZTool.isNotEmptyString(value)) {
            value = ""
        }
        exTimes = exTimes || (30 * 24 * 60 * 60 * 1000);
        const d = new Date();
        d.setTime(d.getTime() + exTimes);
        const expires = "expires=" + d.toGMTString();
        document.cookie = key + "=" + value + "; " + expires;
    },
    // 获取cookie
    getCookie(key) {
        // key 一定要有
        if (!AZTool.isNotEmptyString(key)) return ""; 
        const name = key + "=";
        const ca = document.cookie.split(';');
        for (let i = 0; i < ca.length; i++) {
            const c = ca[i].trim();
            if (c.indexOf(name) === 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    },
    // 清除cookie
    clearCookie(name){
        // key 一定要有
        if (!AZTool.isNotEmptyString(name)) return 
        const exdays = -1 * 24 * 60 * 60 * 1000 ;
        this.setCookie(name,"",exdays);
    },
    getAllCookie(){
        const _docCookies = document.cookie;
        let result = {}
        if(AZTool.isNotEmptyString(_docCookies)){
            const ca = _docCookies.split(';');
            for (let i = 0; i < ca.length; i++) {
                const c = ca[i].trim();
                const splitIdx = c.indexOf("=");
                if (splitIdx > 0) {
                    const keyName = c.substring(0, splitIdx);
                    const keyValue = c.substring(splitIdx + 1,c.length)
                    result[keyName] = keyValue
                }
            }
        }
        return result
    },
}