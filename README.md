# lhhu-react

## Install

```bash
npm install --save lhhu-react
```

## Usage

```jsx
import React  from 'react'

import {BaseViewComponent,LoadingDialog} from  'lhhu-react'
export default class App extends BaseViewComponent {

  componentDidMount() {


  }

  _addDialog=()=>{
      LoadingDialog.show({
          loadingTitle:'加载中。。。',
          hitBackgroundHidden:true,
          loadingImage:require('./loading.gif')
      })
  }

  render() {
    return(
        <div className={'fullScreen'} style={{}}>

            <button onClick={this._addDialog}>test button</button>
            <div>{this._moduleId}</div>
            <div>{window.__Device__}</div>
            <div>IS_IPHONE_X: {window.IS_IPHONE_X?1:0}</div>
        </div>
    )
  }
}

```

## License

MIT © [lhhu-react](https://gitee.com/myLoveJ/lhhu-react.git)

## 介绍
* AZTool,
	* AZTool.isNull_Undefined  判空
	* AZTool.isArray           判断是否为数组
	* AZTool.isString          判断是否为string
	* AZTool.isNumber          判断是否为Number
	* AZTool.isDate            判断是否为Date
	* AZTool.isFunction        判断是否为Function
	* AZTool.isObject          判断是否为Object
	* AZTool.isNotEmptyString  非空String
	* AZTool.isNotEmptyArray   非空Array
	* AZTool.dateTransform     日期格式化dateTransform
	* AZTool.randomString      随机字符串randomString
* md5,
* Loadable 组建懒加载	
* AppEventEmitter,
	app 事件通知，全局使用
	

	```		
		AppEventEmitter.addListener(BasePage.APP_BG_COLOR_CHANGE_EVENT,this.updateBgThemeColor);
		AppEventEmitter.removeListener(BasePage.APP_BG_COLOR_CHANGE_EVENT,this.updateBgThemeColor)
		AppEventEmitter.emit(BasePage.APP_BG_COLOR_CHANGE_EVENT,{opt:'opt'})

	```

* AZAppTimer,

	```
	共用一个（每一秒触发的）定时器触发回调，自身管理定时器的创建和销毁
	使用者只要添加定时器的方法，和移除定时器的方法即可
	节省定时器的资源
	**当定时器中的回调方法全部移除时，会释放定时器**
	
	componentDidMount() {
        AZAppTimer.addTimerListener(this._calTimer) //在定时器中添加定时器回调的方法
    }

    componentWillUnmount() {
        AZAppTimer.removeTimerListener(this._calTimer)//在定时器中移除定时器回调的方法
    }
	// 回调方法
    _calTimer = ()=>{
        this.setState({
            time:this._getTimerStr()
        })
    }
    
	```

* BaseViewComponent,
	* BaseViewComponent
* DialogBase
	* 弹窗的父类，这个主要是为了提供一个思路
	* 在react 中model的实现方法基本是（大家去网上找），导致使用的形式为`<Model>...</Model>`这种形式，在使用的过程中较为麻烦，DialogBase该类实现了一种静态方法调用弹窗的实现

		```jsx
		import { LoadingDialog } from 'lhhu-react'
		LoadingDialog.show({})
		```
* LoadingDialog

	```jsx
	import { LoadingDialog } from 'lhhu-react'
	LoadingDialog.show({})
	```

* AZCatch 捕捉全局异常


	```jsx

		import { AZCatch } from 'lhhu-react'
		// 开启错误捕捉
		AZCatch.useCatch((err,info)=>{
			// 这里可以把异常上传，监听线上问题
			console.log('\n hulinhua--------haha----11-',err);
		})
		//window 是Window 单例实例
		// 绑定到window上方便全局使用，不需要引入AZCatch，当然也可以直接使用AZCatch.globalTryCatch
		window.globalTryCatch = AZCatch.globalTryCatch
		window.globalPromis = AZCatch.globalPromis

		/**
		 AZCatch.globalTryCatch(()=>{
			// code
			},err=>{
				// 错误处理
		})

		// 或者
		window.globalTryCatch(()=>{
				// code
			},err=>{
				// 错误处理
		})
		*/

	```


