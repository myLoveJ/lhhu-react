"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _react = _interopRequireDefault(require("react"));

var _reactDom = _interopRequireDefault(require("react-dom"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var DialogBase = /*#__PURE__*/function () {
  function DialogBase() {
    _classCallCheck(this, DialogBase);
  }

  _createClass(DialogBase, null, [{
    key: "show",
    value: function show(childrenElement) {
      DialogBase.hidden();
      DialogBase.appendLoadingDialogModel(childrenElement);
    }
  }, {
    key: "hidden",
    value: function hidden() {
      if (document.getElementById(DialogBase.Dialog_Key)) {
        // 移除ChildrenElement
        _reactDom["default"].render( /*#__PURE__*/_react["default"].createElement("div", null), document.getElementById('loadingDialog')); // 移除 dialog


        DialogBase.rootDiv.removeChild(document.getElementById(DialogBase.Dialog_Key));
      }
    }
  }, {
    key: "appendLoadingDialogModel",
    value: function appendLoadingDialogModel(ChildrenElement) {
      if (!ChildrenElement) return;
      var newNode = document.createElement(DialogBase.Dialog_Key);
      newNode.id = DialogBase.Dialog_Key;
      newNode.innerHTML = '<div id="loadingDialog"></div>';
      DialogBase.rootDiv.appendChild(newNode);

      _reactDom["default"].render(ChildrenElement, document.getElementById('loadingDialog'));
    }
  }]);

  return DialogBase;
}();

exports["default"] = DialogBase;

_defineProperty(DialogBase, "Dialog_Key", 'loadingDialog-doc');

_defineProperty(DialogBase, "rootDiv", document.getElementById('root'));