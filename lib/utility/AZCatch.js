"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/**
 * @author lhhu
 * @class AZCatch 全局错误捕捉
 * @description react 全局捕捉错误日志封装，持续更新中。。。
 * @description 1、window.onerror ，2、try cathc， 3、componentDidCatch（react 16版本以上）
 */
var AZCatch = function AZCatch() {
  _classCallCheck(this, AZCatch);
};

_defineProperty(AZCatch, "globalTryCatch", function (asyncFunc, catchFunc) {
  try {
    return asyncFunc();
  } catch (err) {
    AZCatch.errorCatch && AZCatch.errorCatch(err);
    catchFunc && catchFunc(err);
  }
});

_defineProperty(AZCatch, "globalPromis", function (promiseFunc) {
  return new Promise(function (resolve, reject) {
    return promiseFunc().then(function (res) {
      resolve(res);
    })["catch"](function (err) {
      AZCatch.errorCatch && AZCatch.errorCatch(err);
      reject(err);
    });
  });
});

if (window) {
  window.onerror = function (msg, url, line, colno, err) {
    AZCatch.errorCatch && AZCatch.errorCatch(err);
  };
}

function errorCatch(err, info) {}
/**
 * @method 启用错误日志配置方法
 * @param {捕捉错误日志的方法} catchFunc ，替换原有的errorCatch方法
 */


function useCatch(catchFunc) {
  Object.assign(AZCatch, {
    errorCatch: catchFunc
  });
}

AZCatch.errorCatch = errorCatch;
AZCatch.useCatch = useCatch;
var _default = AZCatch;
exports["default"] = _default;