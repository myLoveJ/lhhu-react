"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function isUndefined(obj) {
  return typeof obj === 'undefined';
}

function isNull_Undefined(obj) {
  return obj === null || isUndefined(obj);
}

function isArray(obj) {
  return !isNull_Undefined(obj) && _typeof(obj) === 'object' && obj.constructor === Array;
}

function isString(str) {
  return !isNull_Undefined(str) && typeof str === 'string' && str.constructor === String;
}

function isNumber(obj) {
  return !isNull_Undefined(obj) && typeof obj === 'number' && obj.constructor === Number;
}

function isDate(obj) {
  return !isNull_Undefined(obj) && _typeof(obj) === 'object' && obj.constructor === Date;
}

function isFunction(obj) {
  return !isNull_Undefined(obj) && typeof obj === 'function' && obj.constructor === Function;
}

function isObject(obj) {
  return !isNull_Undefined(obj) && _typeof(obj) === 'object' && obj.constructor === Object;
}

function isNotEmptyString(str) {
  return isString(str) && str.length > 0;
}

function isNotEmptyArray(obj) {
  return isArray(obj) && obj.length > 0;
}

function dateTransform(date, fmt) {
  if (!isDate(date)) return null;
  var o = {
    'M+': date.getMonth() + 1,
    //月份
    'd+': date.getDate(),
    //日
    'h+': date.getHours(),
    //小时
    'm+': date.getMinutes(),
    //分
    's+': date.getSeconds(),
    //秒
    'q+': Math.floor((date.getMonth() + 3) / 3),
    //季度
    S: date.getMilliseconds() //毫秒

  };
  if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));

  for (var k in o) {
    if (new RegExp('(' + k + ')').test(fmt)) fmt = fmt.replace(RegExp.$1, RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substr(('' + o[k]).length));
  }

  return fmt;
}
/**
 * @method 获取随机字符串
 * @param len 随机长度
 * @param chars 随机的字符串的范围（在这些字符串里面随机，默认为空）
 * @return {string|string} 随机字符串
 */


function randomString(len, chars) {
  len = len || 32;
  var $chars = chars || "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  var maxPos = $chars.length;
  var result = '';

  for (var i = 0; i < len; i++) {
    result += $chars.charAt(Math.floor(Math.random() * maxPos));
  }

  return result;
} // 字符串是否包含字符串


function stringContain(fStr, cStr) {
  if (isNotEmptyString(fStr)) {
    return fStr.search(cStr) > -1;
  } else {
    return false;
  }
} //获取链接 url 的path


function getQueryPath(url) {
  if (!isNotEmptyString(url)) return "";
  var path = url.substring(0, url.lastIndexOf("?"));
  return path || url;
} // 获取链接url 的参数


function getQueryParams(url) {
  if (!isNotEmptyString(url)) return {};
  var search = url.substring(url.lastIndexOf("?") + 1);
  var obj = {};
  var reg = /([^?&=]+)=([^?&=]*)/g;
  search && search.replace(reg, function (rs, $1, $2) {
    var name = decodeURIComponent($1); //解码

    var val = decodeURIComponent($2); //解码

    val = String(val);
    obj[name] = val;
  });
  return obj;
}
/**
 * @method 首字母大写
 * @param str
 * @return {string|*}
 */


function strCapitalFirstLetter(str) {
  if (isNotEmptyString(str)) {
    return str.slice(0, 1).toUpperCase() + str.slice(1);
  } else {
    return str;
  }
}
/**
 * @method 每个单词首字母大写
 * @param str
 * @return {string|*}
 */


function strCapitalEveryFirstLetter(str) {
  if (isNotEmptyString(str)) {
    return str.replace(/\b(\w)(\w*)/g, function ($0, $1, $2) {
      return $1.toUpperCase() + $2.toLowerCase();
    });
  } else {
    return str || "";
  }
}

function getRandomStr(len) {
  if (len < 1) return '';
  var s = [];
  var hexDigits = '0123456789abcdefghijklmnopqrstuvwsyzABCDEFGHIJKLMNOPQRSTUVWSYZ';

  for (var i = 0; i < len; i++) {
    s[i] = hexDigits.substr(Math.floor(Math.random() * 62), 1);
  }

  return s.join('');
}

function safetyObj(obj) {
  return isObject(obj) ? obj : {};
}

function getTextFiledStr(str) {
  if (isNotEmptyString(str)) {
    var textFiledStr = str.trim().replaceAll("\\s+", " ");
    return textFiledStr || "";
  } else {
    return '';
  }
}
/**
 * @method 模糊搜索
 * @param fStr 父string
 * @param cStr 子string
 * @return {boolean}
 */


function stringFuzzySearch(fStr, cStr) {
  if (isNotEmptyString(fStr)) {
    return fStr.search(cStr) > -1;
  } else {
    return false;
  }
} // 每个首字母大写


function strCapitalEveryLetter(str) {
  if (!isNotEmptyString(str)) {
    return str || "";
  } else {
    return str.replace(/\b(\w)(\w*)/g, function ($0, $1, $2) {
      return $1.toUpperCase() + $2.toLowerCase();
    });
  }
}
/**
 * @method 首字母大写
 * @param str
 * @return {string|*}
 */


function strCapitalLetter(str) {
  if (isNotEmptyString(str)) {
    return str.slice(0, 1).toUpperCase() + str.slice(1);
  } else {
    return str;
  }
}

function isEmptyData(obj) {
  if (isNull_Undefined(obj)) {
    return true;
  }

  if (isString(obj)) {
    return !isNotEmptyString(obj);
  }

  if (isArray(obj)) {
    return !isNotEmptyArray(obj);
  }

  if (isObject(obj)) {
    return Object.keys(obj).length < 1;
  }
}

var AZTool = function AZTool() {
  _classCallCheck(this, AZTool);
};

AZTool.isUndefined = isUndefined;
AZTool.isNull_Undefined = isNull_Undefined;
AZTool.isArray = isArray;
AZTool.isString = isString;
AZTool.isNumber = isNumber;
AZTool.isDate = isDate;
AZTool.isFunction = isFunction;
AZTool.isObject = isObject;
AZTool.isNotEmptyString = isNotEmptyString;
AZTool.isNotEmptyArray = isNotEmptyArray;
AZTool.dateTransform = dateTransform; //日期格式转换

AZTool.randomString = randomString; //获取随机字符串

AZTool.stringContain = stringContain; //字符串是否包含字符串

AZTool.getQueryPath = getQueryPath; //获取链接 url 的path

AZTool.getQueryParams = getQueryParams; //获取链接url 的参数

AZTool.strCapitalFirstLetter = strCapitalFirstLetter; //首字母大写

AZTool.strCapitalEveryFirstLetter = strCapitalEveryFirstLetter; //每个单词首字母大写

AZTool.getRandomStr = getRandomStr; //获取随机字符串

AZTool.safetyObj = safetyObj;
AZTool.getTextFiledStr = getTextFiledStr; //用户输入字符串，去掉前后空格

AZTool.stringFuzzySearch = stringFuzzySearch; //模糊搜索

AZTool.strCapitalEveryLetter = strCapitalEveryLetter; //每个首字母大写

AZTool.strCapitalLetter = strCapitalLetter; //首字母大写

AZTool.isEmptyData = isEmptyData; //空数据

var _default = AZTool;
exports["default"] = _default;