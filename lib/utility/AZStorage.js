"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _AZTool = _interopRequireDefault(require("./AZTool"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function isSupportedStorage(isLocal) {
  var storage = isLocal ? localStorage : sessionStorage;
  if (!storage) return false;
  var testKey = 'test';

  try {
    storage.setItem(testKey, 'testValue');
    storage.removeItem(testKey);
    return true;
  } catch (error) {
    return false;
  }
}

function setItem(key, value, isLocal) {
  if (!isSupportedStorage(isLocal)) return null;
  var storage = isLocal ? localStorage : sessionStorage;
  storage.setItem(key, JSON.stringify(value));
}

function clear(isLocal) {
  if (!isSupportedStorage(isLocal)) return null;
  var storage = isLocal ? localStorage : sessionStorage;
  storage.clear();
}

function getItem(key, isLocal) {
  if (!isSupportedStorage(isLocal)) return null;
  var storage = isLocal ? localStorage : sessionStorage;
  var data = storage.getItem(key);

  if (_AZTool["default"].isNull_Undefined(data)) {
    return null;
  } else {
    return JSON.parse(data);
  }
}

function removeItem(key, isLocal) {
  if (!isSupportedStorage(isLocal)) return null;
  var storage = isLocal ? localStorage : sessionStorage;
  storage.removeItem(key);
}

var AZStorage = function AZStorage() {
  _classCallCheck(this, AZStorage);
};

AZStorage.setItem = setItem;
AZStorage.getItem = getItem;
AZStorage.clear = clear;
AZStorage.removeItem = removeItem;
AZStorage.isSupportedStorage = isSupportedStorage;
var _default = AZStorage;
exports["default"] = _default;