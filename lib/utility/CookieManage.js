"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _AZTool = _interopRequireDefault(require("./AZTool"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = {
  // 设置cookie,默认是30天
  setCookie: function setCookie(key, value, exTimes) {
    // key 一定要有
    if (!_AZTool["default"].isNotEmptyString(key)) return;

    if (!_AZTool["default"].isNotEmptyString(value)) {
      value = "";
    }

    exTimes = exTimes || 30 * 24 * 60 * 60 * 1000;
    var d = new Date();
    d.setTime(d.getTime() + exTimes);
    var expires = "expires=" + d.toGMTString();
    document.cookie = key + "=" + value + "; " + expires;
  },
  // 获取cookie
  getCookie: function getCookie(key) {
    // key 一定要有
    if (!_AZTool["default"].isNotEmptyString(key)) return "";
    var name = key + "=";
    var ca = document.cookie.split(';');

    for (var i = 0; i < ca.length; i++) {
      var c = ca[i].trim();

      if (c.indexOf(name) === 0) {
        return c.substring(name.length, c.length);
      }
    }

    return "";
  },
  // 清除cookie
  clearCookie: function clearCookie(name) {
    // key 一定要有
    if (!_AZTool["default"].isNotEmptyString(name)) return;
    var exdays = -1 * 24 * 60 * 60 * 1000;
    this.setCookie(name, "", exdays);
  },
  getAllCookie: function getAllCookie() {
    var _docCookies = document.cookie;
    var result = {};

    if (_AZTool["default"].isNotEmptyString(_docCookies)) {
      var ca = _docCookies.split(';');

      for (var i = 0; i < ca.length; i++) {
        var c = ca[i].trim();
        var splitIdx = c.indexOf("=");

        if (splitIdx > 0) {
          var keyName = c.substring(0, splitIdx);
          var keyValue = c.substring(splitIdx + 1, c.length);
          result[keyName] = keyValue;
        }
      }
    }

    return result;
  }
};
exports["default"] = _default;