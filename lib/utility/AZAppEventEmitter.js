"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getEmitterMap = function getEmitterMap() {
  var _instance = Instance() || {};

  console.assert(_instance, "AppEventEmitter 未初始化");
  return _instance._eventEmitterMap;
};

var AppEventEmitter = function AppEventEmitter() {
  _classCallCheck(this, AppEventEmitter);

  this._eventEmitterMap = new Map();
};

_defineProperty(AppEventEmitter, "emit", function (key, opt) {
  var _emitMap = getEmitterMap();

  var _callbackArr = _emitMap.get(key) || [];

  _callbackArr.map(function (callback) {
    callback(opt);
  });
});

_defineProperty(AppEventEmitter, "addListener", function (key, callback) {
  var _emitMap = getEmitterMap();

  var _callbackArr = _emitMap.get(key) || [];

  if (!_callbackArr.contains(callback)) {
    _callbackArr.push(callback);

    _emitMap.set(key, _callbackArr);
  }
});

_defineProperty(AppEventEmitter, "removeListener", function (key, callback) {
  var _emitMap = getEmitterMap();

  var _callbackArr = _emitMap.get(key) || [];

  if (_callbackArr.contains(callback)) {
    var _newArr = _callbackArr.filter(function (o) {
      return o != callback;
    }) || [];

    _emitMap.set(key, _newArr);
  }
});

var Instance = function () {
  var serverTimeInstance = null;
  return function () {
    if (!serverTimeInstance) {
      serverTimeInstance = new AppEventEmitter();
    }

    return serverTimeInstance;
  };
}();

var _default = AppEventEmitter;
exports["default"] = _default;