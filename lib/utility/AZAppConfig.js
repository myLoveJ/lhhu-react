"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});

function getDeviceType() {
  var ua = navigator.userAgent.toLocaleLowerCase();
  var agent = ["WeChat", "Android", "iPhone", "SymbianOS", "Windows Phone", "iPad", "iPod"];
  var _device = 'Windows';

  for (var i = 0; i < agent.length; i++) {
    var keyStr = agent[i];

    if (ua.indexOf(keyStr.toLocaleLowerCase()) > 0) {
      _device = keyStr;
      break;
    }
  }

  return _device;
}
/**
 * 数组中是否包含item
 * @param argArray
 * @returns {boolean}
 */


Array.prototype.contains = function (item) {
  for (var index = 0; index < this.length; index++) {
    if (this[index] === item) {
      return true;
    }
  }

  return false;
};

window.__Device__ = getDeviceType();
window.__IOS__ = window.__Device__ === 'iPhone';
window.__ANDROID__ = window.__Device__ === 'Android';
window.__WINDOWS__ = window.__Device__ === 'Windows';
window.IS_IPHONE_X = window.__Device__ === "iPhone" && (window.screen.height === 812 || window.screen.height === 896);