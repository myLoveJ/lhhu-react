"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

var getEmitterMap = function getEmitterMap() {
  var _instance = Instance() || {};

  console.assert(_instance, "AZAppTimer 未初始化");
  return _instance;
};

var AZAppTimer = function AZAppTimer() {
  var _this = this;

  _classCallCheck(this, AZAppTimer);

  _defineProperty(this, "addTimer", function () {
    if (_this._callbacks.length > 0 && _this._interval == null) {
      // 没有定时器，且需要定时器的时候才创建定时器
      _this._interval = setInterval(_this._callBack, 1000);
      console.log('AZAppTimer----开启定时器');
    }
  });

  _defineProperty(this, "cleanTimer", function () {
    if (_this._interval && _this._callbacks.length == 0) {
      // 有定时器并且需要停止定时器
      clearInterval(_this._interval);
      _this._interval = null;
      console.log('AZAppTimer----关闭定时器');
    }
  });

  _defineProperty(this, "_callBack", function () {
    _this._callbacks.map(function (callback) {
      callback();
    });
  });

  _defineProperty(this, "addTimerListener", function (callback) {
    if (!_this._callbacks.contains(callback)) {
      _this._callbacks.push(callback);
    }

    _this.addTimer();
  });

  _defineProperty(this, "removeTimerListener", function (callback) {
    if (_this._callbacks.contains(callback)) {
      _this._callbacks = _this._callbacks.filter(function (o) {
        return o != callback;
      });
    }

    _this.cleanTimer();
  });

  this._callbacks = new Array();
  this._interval = null;
};

_defineProperty(AZAppTimer, "addTimerListener", function (callback) {
  var _instance = getEmitterMap();

  _instance.addTimerListener(callback);
});

_defineProperty(AZAppTimer, "removeTimerListener", function (callback) {
  var _instance = getEmitterMap();

  _instance.removeTimerListener(callback);
});

var Instance = function () {
  var appTimer = null;
  return function () {
    if (!appTimer) {
      appTimer = new AZAppTimer();
    }

    return appTimer;
  };
}();

var _default = AZAppTimer;
exports["default"] = _default;